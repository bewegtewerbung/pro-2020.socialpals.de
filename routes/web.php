<?php


Route::get('/submitted', 'PagesController@submitted');

Route::post('/participate', 'PagesController@participate');


Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/', 'PagesController@admin');

    Route::get('/csv', ['uses' => 'PagesController@exportToCsv']);

});
Route::get('/logout' , 'Auth\LoginController@logout');


Auth::routes(['register' => false]);

Route::get('/{id?}', 'PagesController@home');


<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'pro-2020');


set('repository', 'git@bitbucket.org:bewegtewerbung/{{application}}.socialpals.de.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', []);


host('socialpals.de')
    ->hostname('socialpals')
    ->set('deploy_path', '/kunden/428309_83607/webseiten/socialpals/subdomains/{{application}}')
    ->set('http_user', 'u428309');

task('createDir', function(){
    run('mkdir -p /kunden/428309_83607/webseiten/socialpals/subdomains/{{application}}');
});

task('npm:local', function(){
    run('npm run production');
})->local();

task('upload', function () {
    upload(__DIR__ . "/public/js/", '{{release_path}}/public/js/', [
        'options' => ['--chmod=Du=rwx,Dg=rwx,Do=rx,Fu=rw,Fg=rw,Fo=r']
    ]);
    upload(__DIR__ . "/public/css/", '{{release_path}}/public/css/', [
        'options' => ['--chmod=Du=rwx,Dg=rwx,Do=rx,Fu=rw,Fg=rw,Fo=r']
    ]);
    upload(__DIR__ . "/public/images/", '{{release_path}}/public/images/', [
        'options' => ['--chmod=Du=rwx,Dg=rwx,Do=rx,Fu=rw,Fg=rw,Fo=r']
    ]);
    upload(__DIR__ . "/public/mix-manifest.json", '{{release_path}}/public/mix-manifest.json', [
        'options' => ['--chmod=Du=rwx,Dg=rwx,Do=rx,Fu=rw,Fg=rw,Fo=r']
    ]);
});

task('upload:env', function() {
    upload(__DIR__."/.env.production", '{{deploy_path}}/shared/.env');
});

task('check:env', function(){
    $output = run('cd {{deploy_path}}/shared/ && cat .env');
    writeln('<question>Check database credentials and campaign id!!!</question>');
    writeln($output);
    writeln('<question>... everything alright???</question>');
});


task('deploy', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:vendors',
    'upload',
    'artisan:storage:link',
    'artisan:view:cache',
    'artisan:config:cache',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
]);

before('deploy', 'npm:local');

// FIST DEPLOY

// 1. dep createDir
// 2. dep deploy
// 3. dep upload:env
// 4. dep check:env
// 5. dep artisan:config:cache
// 6. dep artisan:migrate / <Optional>

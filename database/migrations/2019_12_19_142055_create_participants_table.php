<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('campaign_id', 255)->nullable();
            $table->string('salutation', 20)->nullable();
            $table->string('firstname', 255)->nullable();
            $table->string('lastname', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('year_of_birth', 4)->nullable();
            $table->string('size', 255)->nullable();
            $table->string('zipcode', 12)->nullable();
            $table->string('city', 255)->nullable();
            $table->string('country', 2)->nullable();
            $table->text('motivation_1')->nullable();
            $table->text('motivation_2')->nullable();
            $table->string('photo', 255)->nullable();
            $table->string('voucher', 255)->nullable();
            $table->string('partner_id', 255)->nullable();
            $table->string('partner_name', 255)->nullable();
            $table->string('partner_facebookPageId', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participants');
    }
}

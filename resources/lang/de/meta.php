<?php

return [
    'title' => 'Entdecke bei uns die spannenden GraviTrax PRO Neuheiten',
    'description' => 'Das interaktive Kugelbahnsystem - spielerisch Schwerkraft erleben. Im Starter-Set und den 3 Erweiterungen. ',
];

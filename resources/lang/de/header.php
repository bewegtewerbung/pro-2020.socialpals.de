<?php

return [
    'heading' => 'GRIFFIN TUNE S BOA®',
    'intro' => 'JETZT <strong>GRIFFIN TUNE S BOA</strong>® KAUFEN UND GUTSCHEIN FÜR EINEN <strong>1-TAGES-SKIPASS</strong> DER 5 TIROLER GLETSCHER SICHERN*',
    'body' => 'LEKI ist Pionier bei der Entwicklung hochqualitativer Spitzenhandschuhe. Der Griffin Tune S Boa® ist mit dem patentierten Boa® Fit System ausgestattet. Dadurch kannst du die Passform stufenlos einstellen. Einfach am Rad drehen, bis der Handschuh für dich passend anliegt. Das ermöglicht dir die perfekte Kontrolle des Stocks. Das Außenmaterial ist stark wasserabweisend. Die Handfläche besteht aus robustem und rutschfestem Premium Ziegenleder. Dank mf touch ist die Bedienung von Smartphone & Co. problemlos möglich. In Kombination mit dem Trigger S System ist der Griffin Tune S Boa® ein Alpin Handschuh der Extraklasse.'
];

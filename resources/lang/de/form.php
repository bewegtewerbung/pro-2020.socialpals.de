<?php

return [
    'newsletter' => 'Newsletter',
    'description' => 'Erhalte exklusive News, Insider-Informationen und Angebote von uns.',
    'firstname' => 'Vorname',
    'lastname' => 'Nachname',
    'email' => 'E-Mail',
    'agb' => 'Ich akzeptiere die <a href="#teilnahmebedingungen" data-toggle="modal">Teilnahmebedingungen</a> zur Aktion und habe die <a href="https://www.schoeffel.com/de/de/datenschutz" target="_blank">Datenschutzerklärung</a> zur Kenntnis genommen.*',
    'newsletter_brand' => '',
    'newsletter_partner' => 'Ja, ich möchte den E-Mail-Newsletter von :partner abonnieren und zukünftig über Produkte, Angebote und Neuigkeiten informiert werden.',
    'fill_in_description' => 'Gewinnspielzeitraum :startDate - :endDate. Zur Teilnahme einfach das Formular ausfüllen, bis zum :endDate absenden und mit etwas Glück gewinnen. Teilnahme ab 18 Jahren, die Gewinner werden per E-Mail informiert. Wir wünschen allen Teilnehmern viel Glück!',
    'required_field' => '*Pflichtfeld',
    'status' => 'Prüfe bitte Deine Angaben!',
    'response' => [
        'headline' => 'VIELEN DANK!',
        'body' => 'Deine Teilnahme ist erfolgreich bei uns eingegangen. Wenn du auch den Newsletter bestellt hast, haben wir dir eine E-Mail inkl. Link an deine E-Mail-Adresse gesendet. Bitte klicke diesen an, um deine Bestellung zu bestätigen.',
        'note' => 'Falls du keine E-Mail erhältst, sieh bitte in deinem Spam-Ordner nach und füge <u>noreply@socialpals.de</u> zu deinen Kontakt-Adressen hinzu.'
    ]
];


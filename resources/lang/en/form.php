<?php

return [
    'newsletter' => 'Newsletter',
    'description' => 'Receive exclusive news, insider information and offers from us.',
    'firstname' => 'Firstname',
    'lastname' => 'Lastname',
    'email' => 'E-Mail',
    'newsletter_partner' => 'Yes, I would like to subscribe to the e-mail newsletter from <strong>:partner</strong> and be informed about products, offers and news in the future.',
    'required_field' => 'Required field',
    'status' => 'Check your details!',
    'abonnieren' => 'Subscribe now',
    'response' => [
        'headline' => 'Thank you',
        'body' => 'We sent you an e-mail with a link to confirm your e-mail address. Please click on the link to confirm your order.',
        'note' => 'If you have not receive an e-mail, please check your spam folder and add <u>noreply@socialpals.de</u> to your contacts.'
    ]
];
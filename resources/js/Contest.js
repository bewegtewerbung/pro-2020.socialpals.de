import $ from "jquery";

let $photo = $("#photo"),
    $uploadFile = $("#uploadFile"),
    $btnSubmit = $("#btn-submit"),
    $form = $("#formParticipate"),
    $response = $(".form-response");


function submitForm() {
    // $form.on("focusout change", "input,select", function () {
    //     toggleGeneralError(1);
    // });

    //$btnSubmit.on('click', function (e) {
    //e.preventDefault();

    setStatus("processing");

    // safari osx bug for empty file
    // https://stackoverflow.com/questions/49614091/safari-11-1-ajax-xhr-form-submission-fails-when-inputtype-file-is-empty
    let $inputs = $('input[type="file"]:not([disabled])', $form);
    $inputs.each(function (_, input) {
        if (input.files.length > 0) return;
        $(input).prop('disabled', true);
    });

    let formular_data = new FormData($form[0]);

    $inputs.prop('disabled', false);

    let campaignId = $('input[name="campaign_id"]', $form).val(),
        partnerId = $('input[name="partner_id"]', $form).val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: "/participate",
        data: formular_data,
        method: "POST",
        cache: false,
        contentType: false,
        processData: false
    }).done(function (data) {
        if ($.isEmptyObject(data.error)) {
            //window.location.href = '/submitted';
            $form.hide();
            $response.show();
            let myPos = $response.offset().top;
            let $body = $('html, body');
            $body.stop().animate({
                scrollTop: myPos
            }, 500);
        } else {
            showErrors(data.error, data.keys);
            toggleGeneralError();
            setStatus();
        }
    });
    //});

    // $photo.on('change', function () {
    //     let filename = $photo.val();
    //     $uploadFile.html(filename.replace(/^.*\\/, ""));
    // });

}

function toggleGeneralError(hide) {
    let $status = $('.status');
    if (hide) {
        $status.hide();
    } else {
        $status.show();
    }
}

function setInvalidMessage($field, $container, $message) {
    let fieldName = $field.attr("name"),
        $parent = $container ? $container : $field.parent();

    if (!$field.hasClass("is-invalid")) {
        $field.addClass("is-invalid");
    }

    if ($message && $parent.find(".invalid-feedback").length === 0) {
        $parent.append("<div class=\"invalid-feedback\">" + $message + "</div>");
    }
}

function showErrors(messages, fields) {
    $("label").removeClass("err");
    $('input, select').removeClass("is-invalid");
    $form.find(".invalid-feedback").remove();
    $.each(fields, function (key, value) {
        let $fieldName = $("[name='" + fields[key] + "']");
        if ($fieldName.length > 1) {
            let $container = $fieldName.closest('fieldset');
            setInvalidMessage($fieldName, $container, messages[key]);
        } else {
            setInvalidMessage($fieldName, null, messages[key]);
            $("label[for='" + fields[key] + "']").addClass("err");
        }
    });
}

function setStatus(status) {
    let $btnSubmit = $("#btn-submit");
    switch (status) {
        case "processing":
            $btnSubmit.prop("disabled", true).addClass("is-loading");
            break;

        default:
            $btnSubmit.prop("disabled", false).removeClass("is-loading");
            break;
    }
}

export {submitForm};

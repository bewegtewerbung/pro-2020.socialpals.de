let vendor_data=window.global_vendors;
let map;
let initialBounds;
let initedBounds;
let markers=[];
let infowindow;
let vendors=[];
let myObj={};
let openedWindowOnce=false;
let mapsApiKey=window.mapsApiKey;
let webUrlDisabled = window.webUrlDisabled;

function showMarkers() {
    markers.forEach(function (marker) {
        marker.setMap(map);
    });
}

export function filterMarkers(arg, callback) {

    map.fitBounds(initialBounds);

    if (typeof callback === "function") {
        callback();
    }
}


function setMarker() {

    let coordArr, markerSize, markerImageUrl, markerImage, myLatLng, markerTitle;

    let vendorGroups=getVendorLocations(vendors);
    vendorGroups.forEach(function (vendor, index) {

        coordArr=vendor[0]['coordinates'].split(",");
        markerImageUrl='/images/marker@2x.png?v=20200804';
        markerSize=new google.maps.Size(114, 104); // width, height
        markerImage={
            url: markerImageUrl,
            size: markerSize,
            scaledSize: new google.maps.Size(57, 52), // width, height
            origin: new google.maps.Point(0,0),
            anchor: new google.maps.Point(45, 52) //x, y // breite minus Schatten, höhe Marker
        };
        myLatLng=new google.maps.LatLng(Number(coordArr[1]), Number(coordArr[0]));

        initialBounds.extend(myLatLng);

        markerTitle=vendor['vendor'];

        markers[index]=new google.maps.Marker({
            position: myLatLng,
            icon: markerImage,
            title: markerTitle
        });

        setInfoWindow(index, vendor);
    });

    showMarkers();
}

function setInfoWindow(id, vendor) {

    infowindow=new google.maps.InfoWindow({'pixelOffset':new google.maps.Size(-25, 0)});
    google.maps.event.addListener(infowindow, 'domready', function () {

    });
    google.maps.event.addListener(infowindow, 'closeclick', function () {

        map.setOptions({'scrollwheel': false});

    });
    google.maps.event.addListener(markers[id], 'click', function () {

        infowindow.setContent(getInfoWindowContent(vendor));
        infowindow.open(map, markers[id]);
        map.setOptions({'scrollwheel': false});

    });
}

function fixedEncodeURIComponent(str) {
    return encodeURIComponent(str).replace(/[!'()*]/g, function (c) {
        return '%' + c.charCodeAt(0).toString(16);
    });
}

function getInfoWindowContent(vendor) {
    let parameters='';
    let website={'de': 'Webpage', 'fr': 'Site Internet', 'pl': 'Website'};
    let route={
        'de': 'Route auf Google Maps',
        'fr': 'Route sur Google Maps',
        'cz': 'Trasa na mapách Google',
        'en': 'Route on Google Maps',
        'es': 'Ruta en Google Maps',
        'nl': 'Route op Google Maps',
        'it': 'Percorso su Google Maps',
    };
    let contentString='<div class="info-window">';
    vendor.forEach(function (vendor) {
        contentString+=`
        <div class="info-window__item">
            <div class="pb-2 font-weight-bold">${vendor['name']}</div>
            <div>${vendor['street']}</div>
            <div>${vendor['postalCode']} ${vendor['city']}</div>`;
        if (vendor['webUrl'] && !webUrlDisabled) {
            contentString+=`
                <div class="pt-2">
                    <a href="${vendor['webUrl']}" target="_blank">${website[locale]}</a>
                </div>`;
        }
        parameters=`${vendor['street']} ${vendor['postalCode']} ${vendor['city']}`;
        contentString+=`<div class="pt-2"><a href="https://www.google.com/maps/dir/?api=1&destination=${fixedEncodeURIComponent(parameters)}" target="_blank">${route[locale]}</a></div>
        </div>`;
    });
    contentString+='</div>';

    return contentString;
}


let groupBy=function (xs, key) {
    return xs.reduce(function (rv, x) {
        (rv[x[key]]=rv[x[key]] || []).push(x);
        return rv;
    }, {});
};

function getVendorLocations(vendors) {
    let locations=groupBy(vendors, 'coordinates');
    let vendorGroups=[];
    for (let loc in locations) {
        vendorGroups.push(locations[loc]);
    }
    return vendorGroups;
}

export function initialize() {

    if (!window.global_vendors) {
        return false;
    }

    vendors=vendor_data;

    let styles=[
        {
            "featureType": "landscape",
            "stylers": [{"color": "#e0e0e0"}]
        }, {
            "featureType": "poi",
            "stylers": [{"color": "#cccccc"}]
        }, {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [{"color": "#fdfcff"}]
        }, {"featureType": "road", "elementType": "labels", "stylers": [{"saturation": -100}]}, {
            "featureType": "water",
            "stylers": [{"color": "#29abe2"}]
        }, {}];

    let styledMap=new google.maps.StyledMapType(styles, {
        name: ''
    });

    let center={
        'de': ['51.118564', '10.094605'],
        'fr': ['46.885920', '8.019538'],
        'pl': ['52.667633', '19.112854']
    };
    let zoom={'de': 6, 'fr': 8, 'pl': 7};
    let mapOptions={
        scrollwheel: false,
        zoom: 10,
        minZoom: 5,
        maxZoom: 19,
        //center: new google.maps.LatLng(center[locale][0], center[locale][1]),
        center: new google.maps.LatLng(51.118564, 10.094605),
        mapTypeControlOptions: {
            mapTypeIds: ['map_style']
        }
    };
    initialBounds=new google.maps.LatLngBounds();

    map=new google.maps.Map(document.getElementById('mapcanvas'), mapOptions);


    //map.mapTypes.set('map_style', styledMap);
    //map.setMapTypeId('map_style');

    google.maps.event.addListener(map, 'idle', showMarkers);

    google.maps.event.addListener(map, 'tilesloaded', function () {

        initfitBounds();

    });
}

function initfitBounds() {
    if (!initedBounds) {
        setMarker();
        filterMarkers('', function () {
            let z=map.getZoom();
            // set initial zoom to 11 if only one marker is set
            if (z === 19) {
                map.setZoom(11)
            }
        });
        initedBounds=true;
        if (markers.length === 1) {
            google.maps.event.trigger(markers[0], 'click');
        }
    }
}

export function loadScript() {
    let script=document.createElement('script');
    script.type='text/javascript';
    script.src='https://maps.googleapis.com/maps/api/js?v=3.exp' +
        '&key='+mapsApiKey+'&callback=initialize&language=' + locale;
    document.body.appendChild(script);
}





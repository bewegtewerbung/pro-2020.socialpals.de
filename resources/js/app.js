import $ from "jquery";
import Swiper from 'swiper';
import 'bootstrap/dist/js/bootstrap.bundle';

import {Utils} from "./Utils";
import {loadScript as loadMap, initialize} from './map';

window.addEventListener('resize', () => {
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
});

$(function () {

    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);

    window.initialize = initialize;
    if (window.global_vendors) {
        loadMap();
    }

    let videoSrc,
        $videoModal = $('#videoModal'),
        videoAttributes = '';
    $('.video-preview').on('click', function () {
        videoSrc = $(this).data('src');
        videoAttributes = $(this).data('video-attributes');
    });

    $videoModal.on('shown.bs.modal', function () {
        $('#video').attr('src', videoSrc + '?rel=0&showinfo=0&modestbranding=1&autoplay=1' + videoAttributes);
    });

    $videoModal.on('hide.bs.modal', function () {
        $("#video").attr('src', '');
    });


    $("#photo").on('change', function () {
        let filename = $(this).val();
        $("#uploadFile").html(filename.replace(/^.*\\/, ""));
    });

    $('*[data-hide-on-shown="true"]').each(function () {
        let $this = $(this);
        let $target = $($this.data('target'));
        $target.on('shown.bs.collapse', function () {
            $this.hide();
        });
    });

    $('*[data-hide-on-shown="false"]').each(function () {
        let $this = $(this);
        let $target = $($this.data('target'));
        let $bodyHtml = $('html, body')
        $target.on('shown.bs.collapse', function () {
            $this.text('weniger anzeigen');
            $bodyHtml.animate({
                scrollTop: $target.offset().top
            }, 200);
        });
        $target.on('hide.bs.collapse', function () {
            $this.text('mehr erfahren');
        });
    });

    let $scrollTarget = $('#scrollTarget');
    let $btnScrollToTarget = $('#scrollToTarget');
    let originialOffsetTop = $scrollTarget.offset() ? $scrollTarget.offset().top : 0;

    if ($scrollTarget.length) {
        window.addEventListener('load', function () {
            _toggleButtonScrollToForm();
        });
        window.addEventListener("scroll", function () {
            _toggleButtonScrollToForm();
        });
        $btnScrollToTarget.on('click', function () {
            $("html, body").animate({scrollTop: $('#scrollTarget').offset().top}, 400);
        });
    }

    function _toggleButtonScrollToForm() {
        if (!Utils.isAnyPartOfElementInViewport($scrollTarget)) {
            $btnScrollToTarget.removeClass('d-none');
        } else {
            $btnScrollToTarget.addClass('d-none');
        }
    }

    $('.btn-switch').each(function () {
        let $this = $(this),
            $btn = $this.find('.btn-switch__button button'),
            $cont = $this.find('.btn-switch__cont');
        $btn.on('click', function (e) {
            e.preventDefault();
            switch ($(this).index()) {
                case 0:
                    $($btn[0]).addClass('active');
                    $($btn[1]).removeClass('active');
                    $($cont[0]).show();
                    $($cont[1]).hide();
                    break;
                case 1:
                    $($btn[1]).addClass('active');
                    $($btn[0]).removeClass('active');
                    $($cont[1]).show();
                    $($cont[0]).hide();
                    break;
            }
            $(".swiper-container").each(function () {
                let swiper = this.swiper;
                swiper.update();
            });
        })
    });

    // swiper controls outside swiper-container
    let swiperOuter = window.document.getElementsByClassName('swiper-outer');

    for (let i = 0; i < swiperOuter.length; i++) {

        let el = swiperOuter[i];
        let swiperContainer = el.getElementsByClassName('swiper-container')[0];
        let btnNext = el.getElementsByClassName('swiper-button-next')[0];
        let btnPrev = el.getElementsByClassName('swiper-button-prev')[0];

        new Swiper(swiperContainer, {
            autoplay: false,
            loop: true,
            simulateTouch: true,
            navigation: {
                nextEl: btnNext,
                prevEl: btnPrev,
            },
            pagination: {
                el: '.swiper-pagination',
                clickable: true
            },
            breakpointsInverse: true,
            breakpoints: {
                480: {}
            }
        });
    }

});


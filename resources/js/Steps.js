import $ from "jquery";
import 'parsleyjs';
import {submitForm} from "./Contest";

Parsley.options.errorsWrapper = '<label class="invalid-feedback"></label>';
Parsley.options.errorTemplate = '<span></span>';
Parsley.options.errorClass = 'is-invalid';
Parsley.options.successClass = 'is-valid';

Parsley.addValidator('size', {
    requirementType: 'string',
    validateString: function (value, requirement, parsleyField) {
        if(! (parsleyField.$element.val() || $(requirement).val()) ) {
            $('.invalid-form-error-message').html(parsleyField.$element.data('msg'));
            return false;
        }
        $('.invalid-form-error-message').html('');
        return true;
    }
});


let currentPosition,
    _stepsTotal = $('.step').length;

export default function () {
    let $btnNextStep = $('.btn-next-step'),
        $btnPaginate = $('.step-pagination-bullet'),
        $formParticipate = $('#formParticipate');
    setCurrentPosition(1);

    let $steps = $('.step');
    $steps.each(function (index, step) {
        $(step).find(':input').attr('data-parsley-group', 'block-' + (index + 1));
    });

    $btnNextStep.on('click', function () {

        $formParticipate.parsley().whenValidate({
            group: 'block-' + getCurrentPosition()
        }).done(function () {
            if (getCurrentPosition() === _stepsTotal) {
                submitForm();
                return;
            }
            setContent(getNextPosition());
            scrollToFormTop();
        });
    });

    setContent(getCurrentPosition());

    $btnPaginate.on('click', function (e) {
        let myIndex = $(this).index() + 1;
        if (getCurrentPosition() > myIndex) {
            setContent(myIndex);
            setCurrentPosition(myIndex);
            scrollToFormTop();
        }
    });

    $('#sizeF').on('change', function () {
        if ($(this).val() && $('#sizeM').val()) {
            $('#sizeM').prop('selectedIndex', 0);
        }
    });
    $('#sizeM').on('change', function () {
        if ($(this).val() && $('#sizeF').val()) {
            $('#sizeF').prop('selectedIndex', 0);
        }
    });
}

function scrollToFormTop() {
    let position = $('.inner-form').offset().top;
    $("html, body").animate({scrollTop: position}, 400);
}

function setContent(position) {

    $('.steps > .step').removeClass('step-active');
    $('.steps > .step:nth-child(' + position + ')').addClass('step-active');
    setPagination(position);
    setButton(position);

}

function setButton(position) {
    let $btnNextStep = $('.btn-next-step');
    if (position === _stepsTotal) {
        $btnNextStep.text($btnNextStep.data('label-submit'));
        $btnNextStep.attr('id', 'btn-submit');
        return;
    }
    $btnNextStep.text($btnNextStep.data('label-next'));
}

function setPagination(position) {
    $('.step-pagination-bullet').removeClass('step-pagination-bullet-active');
    $('.step-pagination-bullet:nth-child(' + position + ')').addClass('step-pagination-bullet-active');
}

function setCurrentPosition(step) {
    currentPosition = step;
}

function getCurrentPosition() {
    if (!currentPosition) {
        setCurrentPosition(1);
    }
    return currentPosition;
}

function getNextPosition() {
    if (getCurrentPosition() + 1 <= $('.step').length) {
        setCurrentPosition(getCurrentPosition() + 1);
    }
    return getCurrentPosition();
}



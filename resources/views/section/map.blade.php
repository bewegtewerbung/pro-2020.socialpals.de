<div class="section bg-blue py-3 bg-arrow-blue position-relative">
    <div class="container text-center">
        <div class="text-3 font-weight-bold text-white">
           Hier findest Du uns
        </div>
    </div>
</div>

<div class="karte">
    <div id="mapcanvas"></div>
</div>
<!--suppress ES6ConvertVarToLetConst, JSUnusedLocalSymbols -->
<script>

    var global_vendors= @json($partner['addresses']);
    var mapsApiKey='{!! config('services.google.maps_api_key') !!}';
    var webUrlDisabled = false;
    locale='{{app()->getLocale()}}';

</script>

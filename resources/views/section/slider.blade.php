<div class="swiper-outer position-relative">
    <div class="swiper-button-next"></div>
    <div class="swiper-container swiper-container-{{$slider}}">
        <div class="swiper-wrapper">
            @for($i=1;$i<=$countSlides;$i++)
                <div class="swiper-slide">
                    <div class="swiper-slide__body product-{{$slider}}-{{$i}} product-{{app()->getLocale()}}">
                        <div></div>
                    </div>
                    @isset($caption[$i-1])
                        <div class="swiper-slide__caption text-2 text-green font-weight-bold">{!! $caption[$i-1] !!}</div>
                    @endif
                </div>
            @endfor
        </div>
        <div class="swiper-pagination"></div>
    </div>
    <div class="swiper-button-prev"></div>
</div>

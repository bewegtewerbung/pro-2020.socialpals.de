<div class="banner">
    <div class="container text-center pt-4">
        <img class="img-fluid" src="images/logo_gravitrax.png"
             srcset="images/logo_gravitrax.png 1x, images/logo_gravitrax@2x.png 2x" alt="GRAVITRAX">
    </div>
    <div class="banner__body">
        <div class="text-center px-4">
            <div class="bg-rhombus">
                <div>
                    <div class="text-1 text-sm-2 text-lg-4 text-white font-weight-bold px-4 px-sm-5 leading-tight">Jetzt neu – GraviTrax PRO</div>
                </div>
            </div>
        </div>
    </div>
</div>

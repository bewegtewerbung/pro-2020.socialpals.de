
    <div class="container px-0 py-lg-5">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube-nocookie.com/embed/{{$videoId}}?modestbranding=1&enablejsapi=1&rel=0&showinfo=0&origin={{config('app.url')}}"
                allowfullscreen></iframe>
        </div>

    </div>


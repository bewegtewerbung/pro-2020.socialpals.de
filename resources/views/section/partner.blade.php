@if($partner['logoUrl'])
    <div class="section bg-green">
        <div class="container py-3 text-center">
            <img class="img-fluid"
		 src="{{$partner['logoUrl']}}"
	    	 alt="{{$partner['companyAdditionalInformation']}}"
		>
        </div>
    </div>
@endif

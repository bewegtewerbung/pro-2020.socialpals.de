<div class="section bg-forms position-relative">
    <div class="pt-2 pt-lg-3 pb-4">
        <div class="text-dark text-center">
            <div class="py-1"><a class="" href="https://www.ravensburger.de/start/impressum/index.html"
                                 target="_blank">Impressum</a></div>
            <div class="py-1"><a class=""
                                 href="https://www.ravensburger.de/start/datenschutz/index.html"
                                 target="_blank">Datenschutzerklärung Ravensburger</a></div>
            <div class="py-1"><a
                    class="" href="https://www.socialpals.de/datenschutz"
                    target="_blank">Datenschutzerklärung socialPALS</a></div>
            <div class="py-3 text-center">
                <a href="https://www.socialpals.de/" target="_blank"><img class="img-fluid"
                                                                          src="images/logo_socialpals.png?v=20200303"
                                                                          srcset="images/logo_socialpals.png?v=20200303 1x, images/logo_socialpals@2x.png?v=1 2x"
                                                                          alt="socialPALS"></a>
            </div>
        </div>
    </div>
    <div class="position-absolute" style="right:0; bottom:0;"><img src="images/logo_ravensburger.png" srcset="images/logo_ravensburger.png 1x, images/logo_ravensburger@2x.png 2x" alt="Ravensburger"></div>
</div>

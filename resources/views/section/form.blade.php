<div class="section position-relative bg-white py-5">
    <div class="container w-lg-700">
        <div class="text-6 text-center leading-none font-serif font-weight-bold">
            Jetzt mitmachen <br>
            und bewerben
        </div>
        <form id="formParticipate" method="post" novalidate action="/participate">
            {{ csrf_field() }}
            <input type="hidden" name="partner_id" value="{{$partner['id']}}">
            <input type="hidden" name="partner_name" value="{{$partner['companyAdditionalInformation']}}">
            <input type="hidden" name="brand_name" value="{{$brandName}}">
            <input type="hidden" name="partner_facebookPageId" value="{{$partner['facebookPageId']}}">
            <input type="hidden" name="campaign_id" value="{{$campaignId}}">
            <div class="py-2 inner-form">
                <div class="steps">
                    <div class="step step-active">
                        <fieldset>
                            <legend class="font-weight-bold font-serif text-2 text-uppercase py-3 text-center">
                                Persönliche Angaben
                            </legend>
                            <div class="form-group">
                                <div class="custom-radio custom-control custom-control-inline">
                                    <input class="custom-control-input" type="radio" id="salutation_maennlich"
                                           value="Herr" name="salutation" checked>
                                    <label class="custom-control-label" for="salutation_maennlich"> Herr </label>
                                </div>
                                <div class="custom-radio custom-control custom-control-inline">
                                    <input class="custom-control-input" type="radio" id="salutation_weiblich"
                                           value="Frau" name="salutation">
                                    <label class="custom-control-label" for="salutation_weiblich"> Frau </label>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="firstname">Vorname*</label>
                                    <input type="text" class="form-control" id="firstname" name="firstname" value=""
                                           required data-parsley-required-message="{{__('validation.required')}}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="lastname">Nachname*</label>
                                    <input type="text" class="form-control" id="lastname" name="lastname" value=""
                                           required data-parsley-required-message="{{__('validation.required')}}">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="email">E-Mail*</label>
                                    <input type="email" class="form-control" id="email" name="email" value=""
                                           data-parsley-type="email" required
                                           data-parsley-type-message="Bitte trage eine gültige E-Mail-Adresse ein"
                                           data-parsley-required-message="{{__('validation.required')}}">
                                </div>
                                <div class="form-group col-md">
                                    <label for="yearOfBirth">Geburtsjahr*</label>
                                    <input type="number" min="1900" max="2012" class="form-control" id="yearOfBirth" placeholder="z.B. 1971"
                                           name="year_of_birth" value="" required
                                           data-parsley-range-message="Du musst mindestens 18 Jahre alt sein"
                                           data-parsley-required-message="{{__('validation.required')}}">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-12">
                                    <label for="size">Konfektionsgröße</label>
                                    <input type="text" class="form-control" id="size" name="size" value="">
                                </div>
                            </div>
                        </fieldset>
                        <div>{{__('form.required_field')}}</div>
                    </div>

                    <div class="step">
                        <fieldset>
                            <legend class="font-weight-bold font-serif text-2 text-uppercase py-3 text-center">
                                Dein Wohnort
                            </legend>
                            <div class="form-row">
                                <div class="form-group col-4">
                                    <label for="zipcode">PLZ*</label>
                                    <input type="text" class="form-control" id="zipcode" name="zipcode" value=""
                                           required data-parsley-required-message="{{__('validation.required')}}"
                                           maxlength="10">
                                </div>
                                <div class="form-group col-8">
                                    <label for="city">Ort*</label>
                                    <input type="text" class="form-control" id="city" name="city" value="" required
                                           data-parsley-required-message="{{__('validation.required')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="country">Land*</label>
                                <select class="custom-select" id="country" name="country" required
                                        data-parsley-required-message="{{__('validation.required')}}">
                                    <option value="de">Deutschland</option>
                                    <option value="at">Österreich</option>
                                    <option value="ch">Schweiz</option>
                                </select>
                            </div>
                        </fieldset>
                        <div>{{__('form.required_field')}}</div>
                    </div>

                    <div class="step">
                        <fieldset>
                            <legend class="font-weight-bold font-serif text-2 text-uppercase py-3 text-center">
                                Deine Motivation
                            </legend>
                            <div class="form-row">
                                <div class="form-group col-12">
                                    <label for="motivation_1">Was war Deine anspruchsvollste bisherige Skitour?</label>
                                    <input type="text" class="form-control" id="motivation_1" name="motivation_1" value="">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-12">
                                    <label for="motivation_2">Warum möchtest Du an diesem Skitouren Wochenende dabei sein?</label>
                                    <textarea class="form-control" name="motivation_2" id="motivation_2" cols="30"
                                          rows="10"></textarea>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-12 text-center pt-5">
                                    <label for="photo" class="text-center w-100">Lade hier ein Bild Deiner schönsten Skitour hoch!</label>
                                    <div class="text-0">Einfach von Deinem Datenträger auswählen (jpg,&nbsp;png,&nbsp;max.&nbsp;2&nbsp;MB)</div>
                                    <div class="pt-2">
                                        <div>
                                <span class="fileinput-button">
                                    <input type="file" name="photo" id="photo" value="">
                                    <span> </span>
                                    <span class="text-black font-weight-bold" style="text-decoration: underline">Durchsuchen und Bild hochladen!</span>
                                </span>
                                        </div>
                                        <div>
                                            <div id="uploadFile"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>

                    <div class="step">
                        <fieldset>
                            <legend class="font-weight-bold font-serif text-2 text-uppercase py-3 text-center">
                                Teilnahmebedingungen&nbsp;/ Datenschutz
                            </legend>

                            <div class="pb-2">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="oflegalAge"
                                           name="oflegalAge" required
                                           data-parsley-class-handler="#oflegalAge"
                                           data-parsley-required-message="{{__('validation.required_check')}}">
                                    <label class="custom-control-label"
                                           for="oflegalAge">Hiermit bestätige ich, dass ich über
                                        18 Jahre alt bin.* </label>
                                </div>
                            </div>
                            <div class="py-2">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="agb"
                                           name="agb" required
                                           data-parsley-class-handler="#agb"
                                           data-parsley-required-message="{{__('validation.required_check')}}">
                                    <label class="custom-control-label" for="agb">{!! __('form.agb') !!}</label>
                                </div>
                            </div>
                            @if($partner['offerNewsletterSubscription'])
                                <div class="py-2">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="newsletter_partner"
                                               name="newsletter[]"
                                               value="{{$partner['companyAdditionalInformation']}}">
                                        <label class="custom-control-label" for="newsletter_partner">
                                            {!! __('form.newsletter_partner', ['partner' => $partner['companyAdditionalInformation']]) !!}
                                        </label>
                                    </div>
                                </div>
                            @endif

                            @if($voucherEnabled)
                                <div class="py-2">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="voucher"
                                               name="voucher"
                                               value="{{ $voucherCode }}">
                                        <label class="custom-control-label" for="voucher">
                                            Ja, ich möchte per Mail einen 10% Gutschein auf Schöffel Bekleidung. Dieser kann beim nächsten Einkauf im Sportfachhandel eingelöst werden.
                                        </label>
                                    </div>
                                </div>
                            @endif

                            <div class="pt-4 pb-3">
                                {!! __('form.fill_in_description',
                                    ['startDate' => $startDate->format('d.m.'),
                                    'endDate' => $endDate->format('d.m.Y')]) !!}
                            </div>

                            <div>{{__('form.required_field')}}</div>

                            <div class="status">
                                <div class="alert text-center alert-danger mt-3">
                                    <div>{{__('form.status')}}</div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="text-center pt-3 pb-5">
                    <button type="button"
                            class="btn font-serif btn-lg btn-black btn-w-full text-uppercase strong btn-next-step"
                            data-label-submit="Jetzt mitmachen!" data-label-next="weiter">
                        weiter
                    </button>
                </div>
                <div class="step-pagination-bullets text-center">
                    <div class="step-pagination-bullet"></div>
                    <div class="step-pagination-bullet"></div>
                    <div class="step-pagination-bullet"></div>
                    <div class="step-pagination-bullet"></div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="section text-center form-response">
    <div class="container">
        <div class="py-4 w-lg-700 mx-auto">
            <div class="text-2 font-serif text-uppercase pb-2">{{__('form.response.headline')}}</div>
            <div class="pt-2 text-1 text-center ">
                <div>{{__('form.response.body')}}</div>
                <div class="pt-2">{!! __('form.response.note') !!}</div>
            </div>
        </div>
    </div>
</div>




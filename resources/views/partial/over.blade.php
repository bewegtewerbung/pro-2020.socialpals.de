<div class="section">
    <div class="container text-center">
        <div class="py-4 py-lg-5">
            <div class="text-3 text-dark">
                {{__('over.beendet',['endDate'=>$endDate->format('d.m.Y')])}}
            </div>
        </div>
    </div>
</div>

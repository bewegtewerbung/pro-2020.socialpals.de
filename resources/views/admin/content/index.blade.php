@extends('admin/layout')

@section('content')


    @if(session()->has('flash_message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            {{session()->get('flash_message')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="pb-4">
        <a href="/admin/csv" class="btn btn-outline-secondary">Export to CSV</a> <span style="padding-left: 2rem;">Teilnehmer: {{$participantsNumber}}</span>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th>Name</th>
            <th>E-Mail</th>
            <th>Straße</th>
            <th>PLZ Ort</th>
            <th>Partner Id</th>
            <th>Partner Name</th>
            <th>Datum</th>
        </tr>
        </thead>
        <tbody>

        @foreach($participants as $participant)
            <tr>
                <td>{{$participant->firstname}} {{$participant->lastname}}</td>
                <td>{{$participant->email}}</td>
                <td>{{$participant->street}} {{$participant->house_number}}</td>
                <td>{{$participant->zip}} {{$participant->city}}</td>
                <td>{{$participant->partner_id}}</td>
                <td>{{$participant->partner_name}}</td>
                <td>{{$participant->created_at}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $participants->links() }}

@stop

@extends('layouts.app')

@section('title',__('meta.title'))

@section('description',__('meta.description', ['until'=> $endDate->format('d.m.y')]))

@section('content')

    @include('section.header')

    @include('partial.button-apply')

    @include('section.partner')

    <div class="section bg-white">

        <div class="container text-center py-4 py-md-5">
            <img class="img-fluid" src="images/btn-new.png" srcset="images/btn-new.png 1x, images/btn-new@2x.png 2x" alt="NEW">
            <div class="text-1 text-dark pb-2 pt-4">Entdecke die spannenden</div>
            <div class="text-4 text-md-8 text-dark font-weight-bold leading-none pb-2">GraviTrax PRO Neuheiten</div>
            <div class="text-1 text-dark">im Starter- Set und den 3 Erweiterungen</div>

            @include('section.slider', ['slider' => '1', 'countSlides'=> 4, 'caption'=> [ 'PRO Action Stone Mixer',
                'PRO Starter-Set Vertical', 'PRO Erweiterung', 'PRO Action-Stone: Splitter']])
        </div>
    </div>

    <div class="section bg-grey-light">

        <div class="container text-center py-4 py-md-5">

            <div class="text-4 text-md-5 text-green font-weight-bold leading-tight mx-auto w-lg-60 pb-3">
               Das interaktive Kugelbahnsystem – spielerisch Schwerkraft erleben
            </div>

            <picture>
                <source srcset="images/kinder-1010x673.jpg" media="(min-width: 1000px)">
                <source srcset="images/kinder-768x512.jpg" media="(min-width: 760px)">
                <img class="img-fluid" src="data:image/gif;base64,R0lGODlhAQABAAAAADs="
                     srcset="images/kinder-576x384.jpg" alt="">
            </picture>

            <div class="text-2 text-dark pt-4 pt-lg-5 pb-4 mx-auto w-lg-75">
                Noch mehr neue Elemente – noch mehr Möglichkeiten! Baue mit PRO Streckenverläufe in mehreren Ebenen über- und untereinander und führe Strecken durch Säulen hindurch, statt außen herum zu bauen! Konstruiere geschickt und lasse die Kugel auf ihrem Weg Durchfahrtstunnel passieren. Entdecke weitere spannende Teile, die das neue vertikale Bauerlebnis optimal ergänzen.
            </div>
            <div class="text-2 font-weight-bold mx-auto w-lg-75">
                ALLE GraviTrax und GraviTrax PRO Produkte sind übrigens miteinander kombinierbar.
            </div>
        </div>
    </div>

    <div class="section bg-white">
       @include('section.video',['videoId' => 'vb6ZBJ_rws0'])

        <div class="container w-lg-60 text-center pt-4 pt-lg-0 pb-4 pb-lg-5">
            <div class="text-4 text-md-5 text-green font-weight-bold leading-tight pb-3">
                Tauche ein in die Welt von GraviTrax und GraviTrax PRO
            </div>
            <div class="text-2">und entdecke im neuen Trailer die kreativen Möglichkeiten!</div>
        </div>
    </div>


    <div id="scrollTarget">

        @if($partner['addresses'] && !$crawler && !$over || $demo)

            @include('section.map')

        @endif

        <div class="section pb-4 pb-lg-5" style="background-color: #8cbf27">
<div class="typeform-widget" data-url="https://ravensburgerag.typeform.com/to/SEJNvvQZ#herkunft=20_RAG_Gravitrax_Socialpals&userlist=RAVENSBURGER" style="width: 100%; height: 500px;"></div> <script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm", b="https://embed.typeform.com/"; if(!gi.call(d,id)) { js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })() </script>
        </div>

        <div class="section bg-forms">
            <div class="container py-4 py-md-5 text-center">
                <div class="text-6 leading-tight font-weight-bold pt-3 py-3">#GraviTrax</div>
                <div class="text-2 text-grey pb-4 mx-auto w-lg-75">Werde jetzt Teil der Ravensburger GraviTrax Community und poste deinen persönlichen GraviTrax-Moment auf Instagram mit dem Hashtag #GraviTrax</div>
                <div class="elfsight-app-d2978732-63a2-47a6-b609-e9da86ee9060"></div>
                <script src="https://apps.elfsight.com/p/platform.js" defer></script>
            </div>
        </div>

            <div class="section bg-blue py-4">
                <div class="container text-center text-white">
                    <div class="text-4 font-weight-bold">Spielerisch Schwerkraft
erleben</div>
                </div>
            </div>

        @include('section.footer')

    </div>


@endsection

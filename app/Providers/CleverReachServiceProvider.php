<?php

namespace App\Providers;

use App\Http\Clients\CleverReachClient;
use Illuminate\Support\ServiceProvider;

class CleverReachServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CleverReachClient::class, function($app){
            return new CleverReachClient([
                'base_uri' => config('services.cleverreach.base_uri'),
                'timeout' => 20.0
            ]);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

<?php

namespace App\Providers;

use App\Http\Clients\SocialPalsClient;
use Illuminate\Support\ServiceProvider;

class SocialPalsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SocialPalsClient::class, function($app){
            return new SocialPalsClient([
                'base_uri' => config('services.platform.base_uri'),
                'timeout' => 20.0
            ]);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

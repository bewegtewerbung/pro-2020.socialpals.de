<?php


namespace App\Http\Clients;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class CleverReachClient extends Client
{
    private $token;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    public function getAccessToken()
    {
        $tokenCacheKey = 'token_'.md5(config('services.cleverreach.base_uri'));
        if (Cache::has($tokenCacheKey)) {
            $this->token = Cache::get($tokenCacheKey);
        }
        if ($this->token) {
            return $this->token;
        }
        try {
            $response = $this->request(
                'POST',
                'login',
                [
                    'json' => [
                        'grant_type' => 'password',
                        'client_id' => config('services.cleverreach.client_id'),
                        'login' => config('services.cleverreach.username'),
                        'password' => config('services.cleverreach.password')
                    ]
                ]
            );
            $this->token = json_decode($response->getBody());
            Cache::put($tokenCacheKey, $this->token, 3600);
            return $this->token;
        } catch (ClientException $e) {
            Log::error($e->getMessage());
        }
        return null;
    }

    public function sendRequest($url, $query = [], $body = [], $method = 'GET')
    {
        //$handler = fopen(storage_path('logs/guzzle-log.log'),'w');
        try {
            $response = $this->request($method, $url, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '.$this->getAccessToken()
                ],
                'query' => $query,
                'json' => $body,
                'debug' => false
            ]);
            return json_decode($response->getBody(), true);
        } catch (ClientException $e) {
            if($e->getResponse()->getStatusCode() == '400') {
                Log::info($e->getMessage());
            } else {
                Log::error($e->getMessage());
            }
        } catch(RequestException $e) {
            Log::error($e->getMessage());
        } catch(Exception $e) {
            Log::error($e->getMessage());
        }
        return null;
    }
}

<?php


namespace App\Http\Clients;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class SocialPalsClient extends Client
{
    private $token;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    public function getAccessToken()
    {
        $tokenCacheKey = 'token_'.md5(config('services.platform.base_uri'));
        if (Cache::has($tokenCacheKey)) {
            $this->token = Cache::get($tokenCacheKey);
        }
        if ($this->token) {
            return $this->token;
        }
        try {
            $response = $this->request(
                'POST',
                'oauth2/token',
                [
                    'json' => [
                        'grant_type' => 'password',
                        'client_id' => config('services.platform.client_id'),
                        'username' => config('services.platform.username'),
                        'password' => config('services.platform.password'),
                    ]
                ]
            );
            $body = json_decode($response->getBody());
            $expires_in = 3600;
            if (is_object($body)) {
                $this->token = $body->access_token;
                $expires_in = property_exists($body, 'expires_in') ? $body->expires_in : $expires_in;
            } else {
                $this->token = $body;
            }
            Cache::put($tokenCacheKey, $this->token, $expires_in);
            return $this->token;
        } catch (ClientException $e) {
            Log::error($e->getMessage());
        }
        return null;
    }

    public function sendRequest($url, $query = [], $body = [], $method = 'GET')
    {
        //$handler = fopen(storage_path('logs/guzzle-log.log'),'w');
        try {
            $response = $this->request($method, $url, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '.$this->getAccessToken()
                ],
                'query' => $query,
                'json' => $body,
                'debug' => false
            ]);
            return json_decode($response->getBody(), true);
        } catch (ClientException $e) {
            Log::error($e->getMessage());
        } catch (RequestException $e) {
            Log::error($e->getMessage());
        } catch (Exception $e) {
            Log::error($e->getMessage());
        }
        return null;
    }
}

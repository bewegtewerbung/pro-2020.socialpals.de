<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Partner extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->resource['partner']['id'] ?? '',
            'companyName' => $this->resource['partner']['companyName'] ?? '',
            'companyAdditionalInformation' => $this->resource['partner']['companyAdditionalInformation'] ?? '',
            'logoUrl' => $this->resource['partner']['logoUrl'] ?? '',
            'facebookPageName' => $this->resource['partner']['facebookPageName'] ?? '',
            'facebookPageId' => $this->resource['partner']['facebookPageId'] ?? '',
            'currency' => $this->resource['partner']['currency'] ?? '',
            'addresses' => $this->getAddresses(),
            'language' => $this->resource['partner']['language'] ?? 'de_DE',
            'locale' => $this->getLocale(),
            'customFieldLabel' => $this->resource['customFieldLabel'] ?? '',
            'customFieldValue' => $this->resource['customFieldValue'] ?? '',
            'offerNewsletterSubscription' => $this->resource['partner']['offerNewsletterSubscription'] ?? 0,
            'emails' => $this->resource['partner']['emails'] ?? [],
            'phoneNumber' => $this->resource['partner']['phoneNumber'],
            'webUrl' => $this->resource['partner']['webUrl']
        ];
    }

    private function getLocale()
    {
        $locale = [
            'de_DE' => 'de',
            'en_GB' => 'en',
            'cs_CZ' => 'cz',
            'it_IT' => 'it',
            'es_ES' => 'es',
            'fr_FR' => 'fr',
            'nl_NL' => 'nl'
        ];
        return $locale[$this->resource['partner']['language']];
    }

    private function getAddresses()
    {
        $temp = [];
        $addresses = $this->resource['partner']['addresses'] ?? $temp;

        foreach ($addresses as $address) {
            if (empty($address['longitude']) || empty($address['latitude'])) {
                continue;
            }
            $temp[] = [
                'name' => $this->resource['partner']['companyAdditionalInformation'],
                'street' => $address['street'] . ' ' . $address['number'],
                'postalCode' => $address['postalCode'],
                'city' => $address['city'],
                'country' => $address['country'],
                'webUrl' => $this->resource['partner']['webUrl'],
                'coordinates' => $address['longitude'] . ', ' . $address['latitude']
            ];
        }
        return $temp;
    }

}

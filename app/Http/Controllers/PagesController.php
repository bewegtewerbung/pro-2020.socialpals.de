<?php

namespace App\Http\Controllers;

use App\Mail\Voucher;
use App\Participant;
use App\Services\SocialPalsService;
use DateInterval;
use DateTime;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use League\Csv\CannotInsertRecord;
use League\Csv\Exception as LeagueCsvExeption;
use League\Csv\Writer;
use Crawler;

use JD\Cloudder\Facades\Cloudder;


class PagesController extends Controller
{

    public function home(Request $request, SocialPalsService $platform, $id = 4)
    {
        $demo = Auth::user() && Auth::user()->hasRole('demo');

        $endDate = '';
        $startDate = '';
        $partnerId = config('app.default_partner_id');
        $campaignId = config('app.default_campaign_id');
        $over = false;

        $validator = Validator::make([
            'id' => $id,
            'c_id' => $request->input('c_id')
        ], [
            'id' => 'required|integer',
            'c_id' => 'required|integer'
        ]);
        if (!$validator->fails()) {
            $partnerId = $id;
            $campaignId = $request->input('c_id');
        }

        $lp = $platform->landingPage($campaignId, $partnerId);

        app()->setLocale($lp['partner']['locale']);

        Session::put('locale', app()->getLocale());

        try {
            $endDate = new DateTime($lp['campaign']['endDate']);
        } catch (Exception $e) {

        }
        try {
            $startDate = new DateTime($lp['campaign']['startDate']);
        } catch (Exception $e) {

        }

        Session::put('partner', $lp['partner']);

        if ($demo) {
            $endDate = new DateTime();
            // end in 10 Days
            $endDate = $endDate->add(new DateInterval('P10D'));
            // start today
            $startDate = new DateTime();
        }


        if (!empty($endDate)) {
            $over = $endDate->getTimestamp() < (time() - 86400);
        }

        return view('home', [
            'partner' => $lp['partner'],
            'brandName' => $lp['campaign']['brand']['companyName'],
            'campaignId' => $lp['campaign']['id'],
            'startDate' => $startDate, 'endDate' => $endDate,
            'demo' => $demo,
            'over' => $over,
            'crawler' => Crawler::isCrawler()
        ]);
    }

    public function participate(Request $request, SocialPalsService $platform)
    {
        app()->setLocale(Session::get('locale'));

        $validator = Validator::make($request->all(), [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'zipcode' => 'required',
            'city' => 'required',
            'country' => 'required',
            'agb' => 'required'
        ]);


        if ($validator->passes()) {

            $participant = new Participant();
            $participant->salutation = $request->input('salutation');
            $participant->firstname = $request->input('firstname');
            $participant->lastname = $request->input('lastname');
            $participant->email = $request->input('email');
            $participant->year_of_birth = $request->input('year_of_birth');
            $participant->zipcode = $request->input('zipcode');
            $participant->city = $request->input('city');
            $participant->country = $request->input('country');
            $participant->motivation_1 = $request->input('motivation_1');
            $participant->motivation_2 = $request->input('motivation_2');
            $participant->size = $request->input('size');
            $participant->partner_id = $request->input('partner_id');
            $participant->partner_facebookPageId = $request->input('partner_facebookPageId');
            $participant->partner_name = $request->input('partner_name');
            $participant->campaign_id = $request->input('campaign_id');

            if ($request->has('voucher')) {

                $participant->voucher = $request->input('voucher');

                $pdf = Storage::disk('public')->exists('vouchers/voucher-' . $participant->partner_id . '.pdf');

                Mail::to($participant->email)
                    ->bcc(['roland@bewegtewerbung.de'])
                    ->send(new Voucher($participant, $pdf));
            }

            if ($request->hasFile('photo')) {

                $image_name = $request->file('photo')->getRealPath();

                Cloudder::upload($image_name, null, [
                    'folder' => 'schoeffel'
                ]);

                $img_url = Cloudder::show(Cloudder::getPublicId(), [
                    "quality" => "auto",
                    "fetch_format" => "auto"
                ]);

                $participant->photo = $img_url;
            }
            $participant->created_at = date('Y-m-d H:i:s');
            $participant->save();

            if ($request->filled(['campaign_id', 'partner_id'])) {
                $platform->addParicipant(
                    $request->input('campaign_id'),
                    $request->input('partner_id')
                );
            }

            if ($request->has('newsletter')) {
                $checkedNewsletters = $request->input('newsletter');
                $newsletter_partner = '';
                $newsletter_veranstalter = '';
                $brand_name = $request->input('brand_name');

                foreach ($checkedNewsletters as $checkedNewsletter) {
                    if ($checkedNewsletter == $brand_name) {
                        $newsletter_veranstalter = $brand_name;
                    } else {
                        $newsletter_partner = $checkedNewsletter;
                    }
                }

                $newsletter = resolve('App\Services\CleverReachService');
                $newsletter->receiverAdd($participant,
                    $newsletter_partner,
                    $newsletter_veranstalter
                );
            }

            return response()->json(['success' => 'Daten gespeichert']);

        }

        return response()->json([
            'error' => $validator->errors()->all(),
            'keys' => $validator->errors()->keys()
        ]);
    }

    public function admin()
    {
        if (Auth::user()->hasRole('demo')) {
            return redirect('/');
        }
        $participantsNumber = Participant::all()->count();
        $participants = (new Participant)->orderBy('created_at', 'desc')->paginate(50);

        return view('admin.content.index', [
            'participants' => $participants,
            'participantsNumber' => $participantsNumber
        ]);
    }

    public function exportToCsv()
    {
        if (Auth::user()->hasRole('demo')) {
            return redirect('/');
        }
        $participants = (new Participant)->orderBy('created_at', 'desc')->get();

        $writer = Writer::createFromFileObject(new \SplTempFileObject());
        try {
            $writer->setDelimiter(";");
        } catch (LeagueCsvExeption $e) {
        }
        $writer->setNewline("\r\n");
        $writer->setOutputBOM(Writer::BOM_UTF8);

        try {
            $writer->insertOne(Schema::getColumnListing('participants'));
        } catch (CannotInsertRecord $e) {
        }
        foreach ($participants as $participant) {
            try {
                $writer->insertOne($participant->toArray());
            } catch (CannotInsertRecord $e) {
            }
        }
        $writer->output('participants.csv');
    }
}

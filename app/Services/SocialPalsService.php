<?php

namespace App\Services;

use App\Http\Clients\SocialPalsClient;
use App\Http\Resources\Partner;
use App\Http\Resources\Campaign;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class SocialPalsService
{
    private $client;

    public function __construct(SocialPalsClient $client)
    {
        $this->client = $client;
    }

    public function landingPage($campaign_id, $partner_id)
    {
       $endpoint = 'landing_page/campaign/' . $campaign_id . '/partner/' . $partner_id;

       if(Cache::has($endpoint)) {
           $landing_page = Cache::get($endpoint);
       } else {
           $landing_page = $this->client->sendRequest($endpoint);

           if (empty($landing_page)) {
               abort(404);
               return null;
           }
           Cache::put($endpoint, $landing_page, now()->addMinutes(60));
       }
        return [
          'campaign' => Campaign::make($landing_page)->resolve(),
          'partner' => Partner::make($landing_page)->resolve()
        ];
    }




    public function addParicipant($campaign_id, $partner_id)
    {
        $endpoint = 'campaign/' . $campaign_id . '/participant';
        $body = ['partner_id' => $partner_id];
        $resp = $this->client->sendRequest($endpoint, [], $body, 'POST');
        //Log::info('addParticipant', ['partner_id' => $partner_id, $resp]);
    }

}

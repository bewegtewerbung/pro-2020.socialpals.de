<?php

namespace App\Services;

use App\Http\Clients\CleverReachClient;
use App\Participant;
use Illuminate\Support\Facades\Session;

class CleverReachService
{
    private $client;

    public function __construct(CleverReachClient $client)
    {
        $this->client = $client;
    }

    public function getAttributes()
    {
        $endpoint = 'groups/' . config('services.cleverreach.list_id') . '/attributes';
        dump($this->client->sendRequest($endpoint));
    }

    public function receiverAdd(Participant $participant, $nl_partner = '', $nl_veranstalter = '')
    {
        $endpoint = 'groups/' . config('services.cleverreach.list_id') . '/receivers';
        $receiver = [
            'email' => $participant->email,
            'registered' => time(),
            'activated' => 0,
            'source' => config('app.name'),
            'attributes' => [
                'anrede' => $participant->salutation,
                'vorname' => $participant->firstname,
                'nachname' => $participant->lastname,
                'newsletter_veranstalter' => $nl_veranstalter,
                'newsletter_retailer' => $nl_partner,
                'aktions_id' => $participant->campaign_id
            ]
        ];
        $body = $receiver;
        $res = $this->client->sendRequest($endpoint, [], $body, 'POST');
        if (!empty($res['email'])) {
            $this->sendActivationMail($res['email']);
        }
    }

    public function sendActivationMail($email)
    {
        $endpoint = 'forms/' . $this->getFormId() . '/send/activate';
        $body = [
            'email' => $email,
            'doidata' => [
                'user_ip' => $_SERVER['REMOTE_ADDR'],
                'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                'referer' => $_SERVER['HTTP_REFERER']
            ]
        ];
        $this->client->sendRequest($endpoint, [], $body, 'POST');
    }

    private function getFormId()
    {
        $partner = Session::get('partner');
        $formIds = config('services.cleverreach.form_id');
        if (array_key_exists($partner['locale'], $formIds)) {
            return $formIds[$partner['locale']];
        }
        return $formIds['default'];
    }
}

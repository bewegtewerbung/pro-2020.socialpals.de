const mix = require('laravel-mix');
require('laravel-mix-polyfill');

const TargetsPlugin = require("targets-webpack-plugin");

require('laravel-mix-purgecss');
require('laravel-mix-copy-watched');
mix.webpackConfig({
    plugins: [
        new TargetsPlugin({
            browsers: ['last 2 versions', 'chrome >= 41', 'IE 11'],
        })
    ]
});

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .polyfill(
        {
            enabled: mix.inProduction(),
            useBuiltIns: "usage",
            targets: {"ie": 11},
            debug: true,
            corejs: 3
        }
    )
    .copyWatched('resources/images/**/*', 'public/images')
    .setPublicPath('public')
    .extract()
    .version();

if (!mix.inProduction()) {
    mix.options({
        sourcemaps: 'inline-source-map'
    }).browserSync({
        proxy: 'pro-2020.test'
    });
}
if (mix.inProduction()) {
       mix.purgeCss({
            enabled: true,
            whitelistPatterns: [/collapse.*/, /collapsing/, /modal-.*/, /show/, /swiper-.*/, /product-.*/]
        });
}

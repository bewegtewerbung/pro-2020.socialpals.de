<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'platform' => [
        'base_uri' => env('PLATFORM_REST_BASE_URI'),
        'client_secret' => env('PLATFORM_REST_CLIENT_SECRET'),
        'client_id' => env('PLATFORM_REST_CLIENT_ID'),
        'username' => env('PLATFORM_REST_USERNAME'),
        'password' => env('PLATFORM_REST_PASSWORD'),
    ],
    'cleverreach' => [
        'base_uri' => env('CR_REST_BASE_URI'),
        'client_id' => env('CR_REST_CLIENT_ID'),
        'username' => env('CR_REST_USERNAME'),
        'password' => env('CR_REST_PASSWORD'),
        'list_id' => env('CR_LIST_ID'),
        'form_id' => [
            'default' => env('CR_FORM_ID_DEFAULT'),
            'de' => env('CR_FORM_ID_DE'),
        ],
    ],
    'google_analytics' => [
        'tracking_id' => env('GOOGLE_ANALYTICS_TRACKING_ID'),
    ],
    'rollbar' => [
        'access_token' => env('ROLLBAR_TOKEN'),
        'level' => env('ROLLBAR_LEVEL'),
    ],
    'google' => [
        'maps_api_key' => env('GOOGLE_MAPS_API_KEY')
    ]
];
